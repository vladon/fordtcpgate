unit TcpGateLogUnit;

interface

uses
  System.SysUtils, System.StrUtils, Winapi.Windows, IdTcpClient, TcpGateHelpers;

type
  TLogType =
    (
      LOGTYPE_ERROR = 0,
      LOGTYPE_INFO = 1,
      LOGTYPE_DEBUG = 2,
      LOGTYPE_EXTRA = 3
    );

const
  LOGTYPE_ERROR_STRING = 'Error';
  LOGTYPE_INFO_STRING = 'Info';
  LOGTYPE_DEBUG_STRING = 'Debug';
  LOGTYPE_EXTRA_STRING = 'Extra';
  LOGTYPE_STRINGS : array [TLogType] of string = (LOGTYPE_ERROR_STRING, LOGTYPE_INFO_STRING, LOGTYPE_DEBUG_STRING, LOGTYPE_EXTRA_STRING);

var
  hLogFile: THandle;
  LogLevel: Integer;

// 20130130 - logging to tcp
var
  LogToTcp: Boolean;
  LogToHost: string;
  LogToPort: Integer;
  LogTimeout: Integer;
  LogReconnect: Boolean;
  LogReconnectAttempts: Integer;
  LogReconnectAttemptsRemaining: Integer;
  LogTcpClient: TIdTcpClient;
  LogFullFilename: string;

procedure InitializeLog(
  const vFullLogFilename: string;
  vLogLevel: Integer;
  vLogToTcp: Boolean = False;
  const vLogToHost: string = 'localhost';
  vLogToPort: Integer = 65535;
  vLogTimeout: Integer = 1000;
  vLogReconnect: Boolean = False;
  vLogReconnectAttempts: Integer = 0
);

procedure LogString(const name: string; const details: string; LogType: TLogType = LOGTYPE_DEBUG); overload;
procedure LogString(const name: string; LogType: TLogType = LOGTYPE_DEBUG); overload;
procedure LogString(const name: string; const details: string; const ConnectionName: string; LogType: TLogType = LOGTYPE_DEBUG); overload;

// TConnectionLog
type
  TConnectionLog = class
  private
    fFilenameTemplate: string; // without date
    fFilename: string; // with date
    fFile: THandle; // file descriptor variable

    fLogLevel: Integer;

    procedure fSetFilenameWithDate;
    procedure fLogWrite(const s: AnsiString);
  public
    constructor Create(const vFilename: string; vLogLevel: Integer);
    procedure LogString(const name: string; const details: string; LogType: TLogType = LOGTYPE_DEBUG); overload;
    procedure LogString(const name: string; LogType: TLogType = LOGTYPE_DEBUG); overload;
    procedure LogString(const name: string; const details: string; const ConnectionName: string; LogType: TLogType = LOGTYPE_DEBUG); overload;
    destructor Destroy; override;
  end;

implementation

function GetLogFullFilenameWithDate: string;
var
  tNow: TDateTime;
  ext: string;
begin
  tNow := Now();

  ext := ExtractFileExt(LogFullFilename);

  Result := LeftStr(LogFullFilename, Length(LogFullFilename) - Length(ext)) + '-' + FormatDateTime('yyyymmdd', tNow) + ext;
end;

procedure InitializeLog(
  const vFullLogFilename: string;
  vLogLevel: Integer;
  vLogToTcp: Boolean = False;
  const vLogToHost: string = 'localhost';
  vLogToPort: Integer = 65535;
  vLogTimeout: Integer = 1000;
  vLogReconnect: Boolean = False;
  vLogReconnectAttempts: Integer = 0);
var
  vFullLogFilenameWithDate: string;
begin
  LogFullFilename := vFullLogFilename;

  vFullLogFilenameWithDate := GetLogFullFilenameWithDate;

  if FileExists(vFullLogFilename) then
    hLogFile := FileOpen(vFullLogFilenameWithDate, fmOpenReadWrite or fmShareDenyNone)
  else
    hLogFile := FileCreate(vFullLogFilenameWithDate, fmOpenReadWrite or fmShareDenyNone);

  LogLevel := vLogLevel;

  LogToTcp := vLogToTcp;
  if LogToTcp then
  begin
    LogToHost := vLogToHost;
    LogToPort := vLogToPort;
    LogTimeout := vLogTimeout;
    LogReconnect := vLogReconnect;
    LogReconnectAttempts := vLogReconnectAttempts;
    if LogReconnectAttempts = 0 then
      LogReconnectAttempts := High(Integer);

    LogReconnectAttemptsRemaining := LogReconnectAttempts;

    LogTcpClient := TIdTcpClient.Create();
    LogTcpClient.Host := LogToHost;
    LogTcpClient.Port := LogToPort;
    LogTcpClient.ConnectTimeout := LogTimeout;
    LogTcpClient.ReadTimeout := LogTimeout;
    try
      LogTcpClient.Connect;
    except
      if not LogTcpClient.Connected then
        LogReconnectAttemptsRemaining := LogReconnectAttemptsRemaining - 1;
    end;
  end;

  FileSeek(hLogFile, 0, 2);
  FileClose(hLogFile);
end;

procedure LogWrite(const s: AnsiString);
var
  vFullLogFilenameWithDate: string;
begin
  vFullLogFilenameWithDate := GetLogFullFilenameWithDate;

  if FileExists(vFullLogFilenameWithDate) then
    hLogFile := FileOpen(vFullLogFilenameWithDate, fmOpenReadWrite or fmShareDenyNone)
  else
    hLogFile := FileCreate(vFullLogFilenameWithDate, fmOpenReadWrite or fmShareDenyNone);

  FileSeek(hLogFile, 0, 2);

// write to file
  FileWrite(hLogFile, s[1], Length(s));

// write to tcp
  if LogToTcp then
  begin
    if (not LogTcpClient.Connected) and (LogReconnect) and (LogReconnectAttemptsRemaining > 0) then
    begin
      try
        LogTcpClient.Connect;
      except
        LogReconnectAttemptsRemaining := LogReconnectAttemptsRemaining - 1
      end;
    end;

    if LogTcpClient.Connected then
    begin
      LogReconnectAttemptsRemaining := LogReconnectAttempts;
      try
        LogTcpClient.IOHandler.WriteLn(string(s));
      except

      end;
    end;
  end;

  FileClose(hLogFile);

// if debug then Writeln to console
  if System.IsConsole then
    Write(s);
end;

procedure LogString(const name: string; const details: string; LogType: TLogType = LOGTYPE_DEBUG);
var
  sLog: AnsiString;
  tNow: TDateTime;
begin
  if Ord(LogType) <= LogLevel then
  begin
    tNow := Now;
    sLog :=
      AnsiString(Format(
        '%s; %s; %s; %s; %s'+#13#10,
        [
          AnsiString(FormatDateTime('yyyymmdd', tNow)),
          AnsiString(FormatDateTime('hhnnss', tNow)),
          AnsiString(LOGTYPE_STRINGS[LogType]),
          AnsiString(name),
          AnsiString(details)
        ]
      ));

    LogWrite(sLog);
  end;
end;

procedure LogString(const name: string; LogType: TLogType = LOGTYPE_DEBUG);
begin
  LogString(name, '', LogType);
end;

procedure LogString(const name: string; const details: string; const ConnectionName: string; LogType: TLogType = LOGTYPE_DEBUG); overload;
begin
  LogString(ConnectionName + '; ' + name, details, LogType);
end;

{ TConnectionLog }

constructor TConnectionLog.Create(const vFilename: string; vLogLevel: Integer);
begin
  Self.fFilenameTemplate := vFileName;
  Self.fLogLevel := vLogLevel;

  Self.fSetFilenameWithDate;
end;

destructor TConnectionLog.Destroy;
begin
  inherited;
end;

procedure TConnectionLog.fLogWrite(const s: AnsiString);
begin
  Self.fSetFilenameWithDate;

  if FileExists(Self.fFilename) then
    Self.fFile := FileOpen(Self.fFilename, fmOpenReadWrite or fmShareDenyNone)
  else
    Self.fFile := FileCreate(Self.fFilename, fmOpenReadWrite or fmShareDenyNone);

  FileSeek(Self.fFile, 0, 2);

// write to file
  FileWrite(Self.fFile, s[1], Length(s));

  FileClose(Self.fFile);

// if debug then Writeln to console
  if System.IsConsole then
    Write(s);
end;

procedure TConnectionLog.fSetFilenameWithDate;
var
  tNow: TDateTime;
  tExt: string;
begin
  tNow := Now();

  tExt := ExtractFileExt(Self.fFilenameTemplate);

  Self.fFilename := LeftStr(Self.fFilenameTemplate, Length(Self.fFilenameTemplate) - Length(tExt)) + '-' + FormatDateTime('yyyymmdd', tNow) + tExt;
end;

procedure TConnectionLog.LogString(const name, details: string;
  LogType: TLogType);
var
  sLog: AnsiString;
  tNow: TDateTime;
begin
  try
    if Ord(LogType) <= Self.fLogLevel then
    begin
      tNow := Now();
      sLog :=
        AnsiString(
          Format(
            '%s; %s; %s; %s; %s' + #13#10,
            [
              AnsiString(FormatDateTime('yyyymmdd', tNow)),
              AnsiString(FormatDateTime('hhnnss', tNow)),
              AnsiString(LOGTYPE_STRINGS[LogType]),
              AnsiString(name),
              AnsiString(details)
            ]
          )
        );
      Self.fLogWrite(sLog);
    end;
  finally

  end;
end;

procedure TConnectionLog.LogString(const name, details, ConnectionName: string;
  LogType: TLogType);
begin
  Self.LogString(ConnectionName + '; ' + name, details, LogType);
end;

procedure TConnectionLog.LogString(const name: string; LogType: TLogType);
begin
  Self.LogString(name, '', LogType);
end;

initialization

finalization

if hLogFile <> INVALID_HANDLE_VALUE then
begin
  LogString('Closing log file', '', LOGTYPE_DEBUG);
  FileClose(hLogFile);
end;

end.
