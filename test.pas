{$APPTYPE CONSOLE}

uses
	System.SysUtils, System.IOUtils, System.IniFiles;

var
	exename: string;
	ininame: string = 'TcpGate.ini';
	M: TMemIniFile;

begin
	exename := 'TcpGate.log';
	Writeln('ExtractFilePath: ' + ExtractFilePath(exename));
	Writeln('ExtractFileDir: ' + ExtractFileDir(exename));
	Writeln('ExtractFileName: ' + ExtractFileName(exename));
	Writeln('Combine: ' + TPath.Combine(ExtractFilePath(ParamStr(0)), ExtractFilePath(exename)) + ExtractFilename(exename));
	Writeln('GetFullPath: ' + TPath.GetFullPath(exename));

	M := TMemInifile.Create(TPath.GetFullPath(ininame));
	Writeln(M.ReadString('Main', 'ProxyIni', 'not found'));
	Writeln(M.ReadString('Main', 'Log', 'not found'));
	Writeln(M.ReadInteger('Main', 'LogLevel', -1));	
	M.Free;
end.