unit TcpGateServiceUnit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.SvcMgr, Vcl.Dialogs, JclFileUtils;

type
  TTcpGate = class(TService)
    procedure ServiceStart(Sender: TService; var Started: Boolean);
    procedure ServiceStop(Sender: TService; var Stopped: Boolean);
  private
    FFileVersion: string;
    jVFI: TJclFileVersionInfo;
    { Private declarations }
  public
    function GetServiceController: TServiceController; override;
    { Public declarations }
  end;

var
  TcpGate: TTcpGate;

implementation

{$R *.DFM}

uses TcpGateConsoleUnit, TcpGateLogUnit;

procedure ServiceController(CtrlCode: DWord); stdcall;
begin
  TcpGate.Controller(CtrlCode);
end;

function TTcpGate.GetServiceController: TServiceController;
begin
  Result := ServiceController;
end;

procedure TTcpGate.ServiceStart(Sender: TService; var Started: Boolean);
begin
  jVFI := TJclFileVersionInfo.Create(ParamStr(0));
  FFileVersion := jVFI.FileVersion;
  jVFI.Free;

//  Self.LogMessage('call InitializeSettings', 1, 0, 1);

  TcpGateConsoleUnit.InitializeSettings(Self);

//  Self.LogMessage('call LoadProxySettings', 1, 0, 2);

  TcpGateConsoleUnit.LoadProxySettings(Self);

//  Self.LogMessage('call StartWorking', 1, 0, 3);

  TcpGateConsoleUnit.StartWorking(Self);

  LogString('Starting TCP Gate version ' + FFileVersion, LOGTYPE_INFO);
  LogString('Service started', LOGTYPE_INFO);
end;

procedure TTcpGate.ServiceStop(Sender: TService; var Stopped: Boolean);
begin
  TcpGateConsoleUnit.StopWorking(Self);
  LogString('Service stopped', LOGTYPE_INFO);
  LogString('Stopped TCP Gate version ' + FFileVersion, LOGTYPE_INFO);
end;

end.
