program TestFileVersion;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils,
  JclFileUtils;

var
  jFVI: TJclFileVersionInfo;

begin
  try
    jFVI := TJclFileVersionInfo.Create(ParamStr(0));
    Writeln(jFVI.FileVersion);
    Writeln(jFVI.ProductVersion);
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
end.
