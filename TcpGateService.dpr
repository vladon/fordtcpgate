program TcpGateService;

uses
  Vcl.SvcMgr,
  TcpGateServiceUnit in 'TcpGateServiceUnit.pas' {TcpGate: TService},
  TcpGateConsoleUnit in 'TcpGateConsoleUnit.pas',
  TcpGateLogUnit in 'TcpGateLogUnit.pas',
  TcpGateProxyConnection in 'TcpGateProxyConnection.pas';

{$R *.RES}

{$UNDEF DEBUG}
{$DEFINE DEBUGEVENTS}

begin
  if not Application.DelayInitialize or Application.Installing then
    Application.Initialize;
  Application.CreateForm(TTcpGate, TcpGate);
  Application.Run;
end.
