unit TcpGateHelpers;

interface

uses
  SysUtils;

function StrNormalToEscaped(s: string): string;
function StrEscapedToNormal(s: string): string;

implementation

function StrNormalToEscaped(s: string): string;
var
  I: Integer;
begin
  Result := '';
  for I := 1 to Length(s) do
  begin
    Result := Result + '#' + IntToStr(Ord(s[I]));
  end;
end;

function StrEscapedToNormal(s: string): string;
// convert string '#AAA#BBB#CCC' to string with corresponding chars
var
  I: Integer;
  charcodebuf: string;
begin
  Result := '';
  I := 1;
  charcodebuf := '';
  while I <= Length(s) do
  begin
    if s[I] = '#' then
    begin
      if charcodebuf <> '' then
        Result := Result + Chr(StrToInt(charcodebuf));
      charcodebuf := ''
    end
    else
      charcodebuf := charcodebuf + s[I];

    Inc(I);
  end;

  if charcodebuf <> '' then
    Result := Result + Chr(StrToInt(charcodebuf));
end;

end.
