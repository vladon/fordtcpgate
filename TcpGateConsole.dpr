program TcpGateConsole;

{$APPTYPE CONSOLE}

{$R *.res}

{$DEFINE DEBUG}
{$DEFINE LOGVERSION1}

{
 2013-05-30
 [ ] third-party initiated connection
}

uses
  Winapi.Windows,
  System.SysUtils,
  ConnectTo1Cv8 in 'ConnectTo1Cv8.pas',
  TcpGateConsoleUnit in 'TcpGateConsoleUnit.pas',
  TcpGateLogUnit in 'TcpGateLogUnit.pas',
  TcpGateProxyConnection in 'TcpGateProxyConnection.pas',
  V82_TLB in 'V82_TLB.pas',
  TcpGateHelpers in 'TcpGateHelpers.pas',
  TcpGateCommon in 'TcpGateCommon.pas',
  TcpGateControl in 'TcpGateControl.pas';

var
  Msg: Winapi.Windows.TMsg;
  bRet: Integer;

function ConsoleCtrlHandler(dwCtrlType: DWORD): BOOL;
begin
  Result := False;
  LogString('Control Event', 'dwCtrlType = ' + IntToStr(dwCtrlType), LOGTYPE_DEBUG);

  case dwCtrlType of
    Winapi.Windows.CTRL_C_EVENT:
      begin
        LogString('Control event', 'Ctrl+C pressed', LOGTYPE_DEBUG);
      end;

    Winapi.Windows.CTRL_BREAK_EVENT:
      begin
        LogString('Control event', 'Ctrl+Break pressed', LOGTYPE_DEBUG);
      end;

    Winapi.Windows.CTRL_CLOSE_EVENT:
      begin
        LogString('Control event', 'Application close event', LOGTYPE_DEBUG);
      end;

    Winapi.Windows.CTRL_LOGOFF_EVENT:
      begin
        LogString('Control event', 'Logoff event', LOGTYPE_DEBUG);
      end;

    Winapi.Windows.CTRL_SHUTDOWN_EVENT:
      begin
        LogString('Control event', 'Shutdown event', LOGTYPE_DEBUG);
      end;
  end;
end;

begin
  try
    InitializeSettings();
    LoadProxySettings();

    Winapi.Windows.SetConsoleCtrlHandler(@ConsoleCtrlHandler, True);
    StartWorking();

// console application WinAPI message loop
    repeat
      bRet := Integer(GetMessage(Msg, 0, 0, 0));

      if bRet = -1 then
      begin
        // error
        Break;
      end
      else
      begin
        TranslateMessage(Msg);
        DispatchMessage(Msg);
      end;
    until bRet = 0;
  except
    on E: Exception do
      LogString('Exception ' + E.ClassName, E.Message, LOGTYPE_ERROR);
  end;
end.
