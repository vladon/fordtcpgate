unit MainUnit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.SvcMgr, Vcl.Dialogs,
  System.IniFiles, System.IOUtils;

type
  TFordTcpGateService = class(TService)
  private
    { Private declarations }
    fExeName: string;
    fIniFileName: string;
    fIniFile: TMemIniFile;
  public
    function GetServiceController: TServiceController; override;
    { Public declarations }

  end;

var
  FordTcpGateService: TFordTcpGateService;

implementation

procedure ServiceController(CtrlCode: DWord); stdcall;
begin
  FordTcpGateService.Controller(CtrlCode);
end;

function TFordTcpGateService.GetServiceController: TServiceController;
begin
  Result := ServiceController;
end;

end.
