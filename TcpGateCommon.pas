unit TcpGateCommon;

interface

const
  MSG_RESPONSE_TIMEOUT = 'RESPONSE TIMEOUT';
  MSG_TRANSIT_TRAFFIC = 'Transit traffic';

  MSG_STATUS_OPEN = 'Open';
  MSG_STATUS_CLOSED = 'Closed';

implementation

end.
