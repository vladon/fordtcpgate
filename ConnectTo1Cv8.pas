unit ConnectTo1Cv8;

{$M+}

interface

uses
  V82_TLB, System.Variants, System.SysUtils, System.Win.ComObj;

const
  DEFAULT_1CV8_VERSION = '8.2';

type
  EVersionInvalid = class(Exception);

type
  TConnectTo1Cv8 = class
  private
    // 1C version
    fVersion: string;

    // fIsActive: is connected to 1cv8
    fIsActive: Boolean;

    // fIsVisible: is 1cv8 window visible
    // no sense if fIsCOM = True
    fIsVisible: Boolean;

    // fIsCOM:
    //   True - this is COM connection
    //   False - this is OLE Automation Client
    fIsCOM: Boolean;

    // f1Cv8: pointer to 1C v8 OLE / COM server
    f1Cv8: IDispatch;

    //////////////////////
    // 1C initialization

    // 1C initialization string
    fInitStr: string;

    // 1C application name (as OLE Automation Server)
    fAppName: string;

    // 1C user
    fUsername: string;

    // 1C password
    fPassword: string;

    // fIsFileOperationMode
    //    True - file operation mode
    //    False - client-server operation mode
    fIsFileMode: Boolean;

    // Fully qualified path to infobase (file operation mode)
    fInfobasePath: string;

    // Server host name
    fServerName: string;

    // Database name
    fDatabaseName: string;

    // chooses AppName according to settings
    procedure ChooseAppName;

  protected
    // Builds init string
    procedure BuildInitStr;

    // Connect/disconnect to 1Cv8
    procedure mSetActive(Value: Boolean);

    // Show/hide app window (only for OLE)
    procedure mSetVisible(Value: Boolean);

    // Set/unset COM
    procedure mSetCOM(Value: Boolean);

    // Set app name
    procedure mSetAppName(Value: string);

    // Set username
    procedure mSetUsername(Value: string);

    // Set password
    procedure mSetPassword(Value: string);

    // Set/unset file mode
    procedure mSetFileMode(Value: Boolean);

    // Set infobase path
    procedure mSetInfobasePath(Value: string);

    // Set server name
    procedure mSetServerName(Value: string);

    // Set database name
    procedure mSetDatabaseName(Value: string);
  public
    // read-only f1Cv8 - pointer to connected database
    property V8: IDispatch read f1Cv8;

    // read-only InitStr
    property InitStr: string read fInitStr;

    // constructors
    constructor Create; overload;
    constructor CreateAndConnectToServerCOM(const cServerName, cDatabaseName, cUsername, cPassword: string); overload;
    constructor CreateAndConnectByInitStr(const pInitStr: string); overload;

    // destructor
    destructor Destroy; override;
  published
    property Version: string read fVersion;
    property IsActive: Boolean read fIsActive write mSetActive;
    property IsVisible: Boolean read fIsVisible write mSetVisible;
    property IsCOM: Boolean read fIsCOM write mSetCOM;
    property AppName: string read fAppName write mSetAppName;
    property Username: string read fUsername write mSetUsername;
    property Password: string read fPassword write mSetPassword;
    property IsFileMode: Boolean read fIsFileMode write mSetFileMode;
    property InfobasePath: string read fInfobasePath write mSetInfobasePath;
    property ServerName: string read fServerName write mSetServerName;
    property DatabaseName: string read fDatabaseName write mSetDatabaseName;
  end;

// helpers
function ConnectTo1Cv8_OLE(s1cAppProgID, InitLine: string; Visible: boolean) : Variant;
function ConnectTo1Cv8_COM(s1cAppProgID, InitLine : string) : Variant;
function IsCorrectVariant(hAny: Variant): Boolean;
function BuildAppName(const Version: string; IsCOM: Boolean): string;

implementation

{ TConnectTo1Cv8 }

uses TcpGateLogUnit;

// constructor

constructor TConnectTo1Cv8.Create;
begin
  inherited;

  fVersion := DEFAULT_1CV8_VERSION;

  f1Cv8 := System.Variants.Unassigned;
  fIsActive := False;
  fIsVisible := False;

  fIsCOM := True;
  fIsFileMode := False;
  fAppName := '';
  if Length(fAppName) <= 0 then
    ChooseAppName;

  fInfobasePath := '';
  fServerName := '';
  fDatabaseName := '';
  fUsername := '';
  fPassword := '';

  BuildInitStr;
end;

constructor TConnectTo1Cv8.CreateAndConnectToServerCOM(const cServerName,
  cDatabaseName, cUsername, cPassword: string);
begin
  Self.Create;

  fServerName := cServerName;
  fDatabaseName := cDatabaseName;
  fUsername := cUsername;
  fPassword := cPassword;

  BuildInitStr;

  Self.fIsCOM := True;

  Self.mSetActive(True);
end;

constructor TConnectTo1Cv8.CreateAndConnectByInitStr(const pInitStr: string);
var
  Splitted: TArray<string>;
  I: Integer;
begin
//  LogString('in CreateAndConnectByInitStr', 'InitStr = ' + pInitStr, LOGTYPE_DEBUG);

  Self.Create;

  Splitted := pInitStr.Trim(['"']).Split([';']);

  for I := Low(Splitted) to High(Splitted) do
  begin
//    LogString('Splitted[' + IntToStr(I) + ']', Splitted[I], LOGTYPE_DEBUG);

    Splitted[I] := Trim(Splitted[I]);

    if Splitted[I].StartsWith('Srvr=', True) then
      fServerName := Splitted[I].Substring(5);

    if Splitted[I].StartsWith('Ref=', True) then
      fDatabaseName := Splitted[I].Substring(4);

    if Splitted[I].StartsWith('Usr=', True) then
      fUsername := Splitted[I].Substring(4);

    if Splitted[I].StartsWith('Pwd=', True) then
      fPassword := Splitted[I].Substring(4);
  end;

  Self.BuildInitStr;

//  LogString('InitStr', Self.fInitStr, LOGTYPE_DEBUG);

  Self.fIsCOM := True;

  Self.mSetActive(True);
end;

destructor TConnectTo1Cv8.Destroy;
begin
  // disconnect
  f1Cv8 := System.Variants.Unassigned;

  fIsActive := False;
  fIsVisible := False;

  inherited;
end;

// chooses app name
procedure TConnectTo1Cv8.ChooseAppName;
begin
  fAppName := BuildAppName(fVersion, fIsCOM);
  if fAppName = '' then
  begin
    raise EVersionInvalid.Create('Specified 1C version is invalid');
  end;
end;

// builds init string
procedure TConnectTo1Cv8.BuildInitStr;
begin
  fInitStr := '';

  if Length(fAppName) <= 0 then
    ChooseAppName;

  fUsername := Trim(fUsername);
  fPassword := Trim(fPassword);
  fInfobasePath := Trim(fInfobasePath);
  fServerName := Trim(fServerName);
  fDatabaseName := Trim(fDatabaseName);

  if fIsFileMode then
  begin
    // this is file infobase
    if Length(fInfobasePath) > 0 then
      fInitStr := fInitStr + ' File=' + '"' + fInfobasePath + '";';
  end else begin
    // client-server infobase
    if Length(fServerName) > 0 then
      fInitStr := fInitStr + ' Srvr=' + '"' + fServerName + '";';
    if Length(fDatabaseName) > 0 then
      fInitStr := fInitStr + ' Ref=' + '"' + fDatabaseName + '";';
  end;

  if Length(fUsername) > 0 then
    fInitStr := fInitStr + ' Usr=' + '"' + fUsername + '";';
  if Length(fPassword) > 0 then
    fInitStr := fInitStr + ' Pwd=' + '"' + fPassword + '";';

  fInitStr := Trim(fInitStr);
end;

// connect/disconnect to 1C
procedure TConnectTo1Cv8.mSetActive(Value: Boolean);
begin
  // returns if nothing changed
  if Value = fIsActive then
    Exit;

  BuildInitStr;
  if Value then
    try
      if fIsCOM then
      begin
        // COM connection
        f1Cv8 := ConnectTo1Cv8_COM(fAppName, fInitStr);
        fIsActive := IsCorrectVariant(f1Cv8);
      end else begin
        // OLE connection
        f1Cv8 := ConnectTo1Cv8_OLE(fAppName, fInitStr, True);
        if IsCorrectVariant(f1Cv8) then
        begin
          fIsActive := True;
          fIsVisible := True;
        end;
      end;
    finally

    end;
end;

procedure TConnectTo1Cv8.mSetAppName(Value: string);
begin
  f1Cv8 := System.Variants.Unassigned;
  fIsActive := False;
  fIsVisible := False;
  fAppName := Trim(Value);
  BuildInitStr;
end;

procedure TConnectTo1Cv8.mSetCOM(Value: Boolean);
begin
  f1Cv8 := System.Variants.Unassigned;
  fIsActive := False;
  fIsVisible := False;
  fIsCOM := IsCOM;

  if fIsCOM then
    ChooseAppName;

  BuildInitStr;
end;

procedure TConnectTo1Cv8.mSetDatabaseName(Value: string);
begin
  f1Cv8 := System.Variants.Unassigned;
  fIsActive := False;
  fIsVisible := False;
  fDatabaseName := Trim(Value);
  BuildInitStr;
end;

procedure TConnectTo1Cv8.mSetFileMode(Value: Boolean);
begin
  f1Cv8 := System.Variants.Unassigned;
  fIsActive := False;
  fIsVisible := False;
  fIsFileMode := Value;
  BuildInitStr;
end;

procedure TConnectTo1Cv8.mSetInfobasePath(Value: string);
begin
  f1Cv8 := System.Variants.Unassigned;
  fIsActive := False;
  fIsVisible := False;
  fInfobasePath := Trim(InfobasePath);
  BuildInitStr;
end;

procedure TConnectTo1Cv8.mSetPassword(Value: string);
begin
  f1Cv8 := System.Variants.Unassigned;
  fIsActive := False;
  fIsVisible := False;
  fPassword := Trim(Value);
  BuildInitStr;
end;

procedure TConnectTo1Cv8.mSetServerName(Value: string);
begin
  f1Cv8 := System.Variants.Unassigned;
  fIsActive := False;
  fIsVisible := False;
  fServerName := Trim(Value);
  BuildInitStr;
end;

procedure TConnectTo1Cv8.mSetUsername(Value: string);
begin
  f1Cv8 := System.Variants.Unassigned;
  fIsActive := False;
  fIsVisible := False;
  fUsername := Trim(Value);
  BuildInitStr;
end;

procedure TConnectTo1Cv8.mSetVisible(Value: Boolean);
begin
  fIsVisible := False;
  if IsCorrectVariant(f1Cv8) then
    if not fIsCOM then
    begin
      fIsVisible := True;
//      f1Cv8.Visible := True;
    end;
end;

// helper functions

// connect to 1Cv8 via OLE
// !!! was not tested !!!
function ConnectTo1Cv8_OLE(s1cAppProgID, InitLine: string; Visible: boolean) : Variant;
begin
  Result := System.Variants.Unassigned;

  s1cAppProgID:=Trim(s1cAppProgID);

  if Length(s1cAppProgID) <= 0 then
    s1cAppProgID:='V82.Application';

  InitLine:=Trim(InitLine);

  if Length(s1cAppProgID)>0 then begin
     // Create connection

     Result := CreateOleObject(s1cAppProgID);

     // Check connection
     if IsCorrectVariant(Result) then begin
        // Initializing 1Cv8
        Result.Connect(InitLine);

        // Set 1Cv8 window visibility

        Result.Visible:=Visible;
     end;
  end;
end;

// connect to 1Cv8 via COM
function ConnectTo1Cv8_COM(s1cAppProgID, InitLine : string) : Variant;
begin
  Result := System.Variants.Unassigned;

  s1cAppProgID:=Trim(s1cAppProgID);

  if Length(s1cAppProgID) <= 0 then
    s1cAppProgID := BuildAppName(DEFAULT_1CV8_VERSION, True);

  InitLine:=Trim(InitLine);

  if Length(s1cAppProgID)>0 then begin
    // Create connection
    Result := CreateOleObject(s1cAppProgID) as IV8COMConnector;
    // Check connection
    if IsCorrectVariant(Result) then begin
      // Initializing 1Cv8
      Result := Result.Connect(InitLine);
    end;
  end;
end;

// check if variant is correct (not empty, ...)
function IsCorrectVariant(hAny: Variant): Boolean;
begin
  Result := not VarIsEmpty(hAny);
end;

// Build COM/OLE App Name
function BuildAppName(const Version: string; IsCOM: Boolean): string;
begin
  if (Version = '8.0') or (Version = '8.1') or (Version = '8.2') then
  begin
    if Version = '8.0' then
      Result := 'V8.';
    if Version = '8.1' then
      Result := 'V81.';
    if Version = '8.2' then
      Result := 'V82.';
  end else
  begin
    Result := '';
  end;

  if IsCOM then
    Result := Result + 'COMConnector'
  else
    Result := Result + 'Application';
end;

initialization

end.
