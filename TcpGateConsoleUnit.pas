unit TcpGateConsoleUnit;

interface

uses
  System.SysUtils, System.IOUtils, System.IniFiles, System.Classes, System.Variants,
  TcpGateLogUnit, TcpGateProxyConnection, System.StrUtils, System.Types,
  IdGlobal, IdTcpServer, IdTcpClient, IdContext, IdSocketHandle,
  Vcl.SvcMgr, TcpGateHelpers, TcpGateCommon;

const
  MAIN_INI_FILENAME = 'TcpGate.ini';
  DEFAULT_PROXY_INI_FILENAME = 'TcpGateProxy.ini';
  DEFAULT_LOG_FILENAME = 'TcpGate.log';

  DEFAULT_LOG_LEVEL = 2;
//  DEFAULT_LOG_LEVEL = 1;

// several constants
const
  CONNECTION_PREFIX = 'Connection ';
  PROXYTYPE_TCPTOTCP = 'TcpToTcp';
  PROXYTYPE_TCPTO1C = 'TcpTo1C';
  PROXYTYPE_INCORRECT = 'Incorrect';
  INCORRECT_LISTENPORT = -1;
  INCORRECT_OUTSERVER = 'INCORRECT.OUTSERVER';
  INCORRECT_OUTPORT = -1;
  INCORRECT_INITSTR = 'INITSTR_INCORRECT';
  INCORRECT_CALLBACKFUNCTION = 'INCORRECT_CALLBACKFUNCTION';

// Identifiers, INI file
const
  IDENT_NAME = 'Name';
  IDENT_PROXYTYPE = 'ProxyType';
  IDENT_LISTENIP = 'ListenIP';
  IDENT_LISTENPORT = 'ListenPort';
  IDENT_OUTSERVER = 'OutServer';
  IDENT_OUTPORT = 'OutPort';
  IDENT_RESTOREIFDISCONNECT = 'RestoreIfDisconnect';
  IDENT_RESTOREATTEMPTS = 'RestoreAttempts';
  IDENT_PASSTHROUGH = 'Passthrough';
  IDENT_ENDOFMESSAGE = 'EndOfMessage';
  IDENT_SENDENDOFMESSAGE = 'SendEndOfMessage';
  IDENT_INITSTR = 'InitStr';
  IDENT_CALLBACKFUNCTION = 'CallbackFunction';
  IDENT_CONVERTSTRING = 'ConvertString';
  IDENT_RESPONSETIMEOUT = 'ResponseTimeout';
  IDENT_CONNECTIONCHECKPERIOD = 'ConnectionCheckPeriod';
  IDENT_LOGFILENAME = 'LogFilename';

  IDENT_PERSISTENTCONNECTION = 'PersistentConnection';

  IDENT_INBOUNDTIMEOUT = 'InboundTimeout';
  IDENT_DROPINBOUNDBUFFERONTIMEOUT = 'DropInboundBufferOnTimeout';
  IDENT_PROCESSINBOUNDBUFFERONTIMEOUT = 'ProcessInboundBufferOnTimeout';

  IDENT_ONSHUTDOWNINBOUNDTIMEOUT = 'OnShutdownInboundTimeout';
  IDENT_ONSHUTDOWNOUTBOUNDTIMEOUT = 'OnShutdownOutboundTimeout';
  IDENT_ONSHUTDOWNSENDINBOUNDBUFFER = 'OnShutdownSendInboundBuffer';
  IDENT_ONSHUTDOWNSENDOUTBOUNDBUFFER = 'OnShutdownSendOutboundBuffer';

  // Listen outbound direction
  IDENT_LISTENOUTBOUND = 'ListenOutbound';
  IDENT_LISTENOUTBOUNDIP = 'ListenOutboundIP';
  IDENT_LISTENOUTBOUNDPORT = 'ListenOutboundPort';
  IDENT_LISTENOUTBOUNDSTATUSSTRING = 'ListenOutboundStatusString';

// Defaults
const
  DEFAULT_LISTENIP = '0.0.0.0';
  DEFAULT_LISTENPORT = INCORRECT_LISTENPORT;
  DEFAULT_OUTSERVER = INCORRECT_OUTSERVER;
  DEFAULT_OUTPORT = INCORRECT_OUTPORT;
  DEFAULT_RESTOREIFDISCONNECT = True;
  DEFAULT_RESTOREATTEMPTS = High(Integer);
  DEFAULT_PASSTHROUGH = False;
  DEFAULT_INITSTR = INCORRECT_INITSTR;
  DEFAULT_CALLBACKFUNCTION = INCORRECT_CALLBACKFUNCTION;
  DEFAULT_CONVERTSTRING = True;
  DEFAULT_ENDOFMESSAGE: string = #13#10;
  DEFAULT_SENDENDOFMESSAGE = False;
  DEFAULT_RESPONSETIMEOUT = 10;
  DEFAULT_CONNECTIONCHECKPERIOD = 10;

  DEFAULT_PERSISTENTCONNECTION = True;

  DEFAULT_INBOUNDTIMEOUT = 1000;
  DEFAULT_DROPINBOUNDBUFFERONTIMEOUT = True;
  DEFAULT_PROCESSINBOUNDBUFFERONTIMEOUT = False;

  DEFAULT_ONSHUTDOWNINBOUNDTIMEOUT = 10;
  DEFAULT_ONSHUTDOWNOUTBOUNDTIMEOUT = 10;
  DEFAULT_ONSHUTDOWNSENDINBOUNDBUFFER = True;
  DEFAULT_ONSHUTDOWNSENDOUTBOUNDBUFFER = True;

  DEFAULT_LISTENOUTBOUND = False;
  DEFAULT_LISTENOUTBOUNDIP = '0.0.0.0';
  DEFAULT_LISTENOUBOUNDPORT = INCORRECT_OUTPORT;
  DEFAULT_LISTENOUTBOUNDSTATUSSTRING = 'STATUS'; // answer: STATUS_OPEN or STATUS_CLOSED

var
  ExeName: string;
  MainIniFilename: string;
  MainIniObject: TMemIniFile;
  ProxyIniFilename: string;
  ProxyIniObject: TMemIniFile;
  LogFilename: string;
  GlobalLogFullFilenameTemplate: string;
  GlobalLogLevel: Integer;
  ProxyConnectionsList: TList;

procedure InitializeSettings(AOwnerService: Vcl.SvcMgr.TService = nil);
procedure LoadProxySettings(AOwnerService: Vcl.SvcMgr.TService = nil);
procedure StartWorking(AOwnerService: Vcl.SvcMgr.TService = nil);
procedure StopWorking(AOwnerService: Vcl.SvcMgr.TService = nil);

implementation

uses ConnectTo1Cv8;

procedure InitializeSettings(AOwnerService: Vcl.SvcMgr.TService = nil);
var
  tLogToTcp: Boolean;
  tLogToHost: string;
  tLogToPort: Integer;
  tLogTimeout: Integer;
  tLogReconnect: Boolean;
  tLogReconnectAttempts: Integer;
begin
  ExeName := ParamStr(0);

  ChDir(TPath.GetDirectoryName(ExeName));

  MainIniFilename := TPath.GetFullPath(MAIN_INI_FILENAME);

  MainIniObject := TMemIniFile.Create(MainIniFilename);

  ProxyIniFilename := MainIniObject.ReadString('Main', 'ProxyIni', DEFAULT_PROXY_INI_FILENAME);

  LogFilename := MainIniObject.ReadString('Main', 'Log', DEFAULT_LOG_FILENAME);

  tLogToTcp := MainIniObject.ReadBool('Main', 'LogToTcp', False);

  if tLogToTcp then
  begin
    tLogToHost := MainIniObject.ReadString('Main', 'LogToHost', 'localhost');
    tLogToPort := MainIniObject.ReadInteger('Main', 'LogToPort', 65535);
    tLogTimeout := MainIniObject.ReadInteger('Main', 'LogTimeout', 1000);
    tLogReconnect := MainIniObject.ReadBool('Main', 'LogReconnect', True);
    tLogReconnectAttempts := MainIniObject.ReadInteger('Main', 'LogReconnectAttempts', 0);
  end else begin
    tLogToHost := 'localhost';
    tLogToPort := 65535;
    tLogTimeout := 1000;
    tLogReconnect := True;
    tLogReconnectAttempts := 0;
  end;

  GlobalLogFullFilenameTemplate := TPath.GetFullPath(LogFilename);
  GlobalLogLevel := MainIniObject.ReadInteger('Main', 'LogLevel', DEFAULT_LOG_LEVEL);

  InitializeLog(
    GlobalLogFullFilenameTemplate,
    GlobalLogLevel,
    tLogToTcp,
    tLogToHost,
    tLogToPort,
    tLogTimeout,
    tLogReconnect,
    tLogReconnectAttempts
  );
  LogString('Start logging', TPath.GetFullPath(LogFilename), LOGTYPE_DEBUG);
  MainIniObject.Free;
end;

procedure LoadProxySettings(AOwnerService: Vcl.SvcMgr.TService = nil);
var
  AllSections: TStringList;
  I: Integer;
  iSectionName: string;
//  iSectionCount: Integer;
  iCurrentListIndex: Integer;
  sCurrentProxyTypeString: string;
  vCurrentProxyType: TProxyType;
  bSkipThisSection: Boolean;
  tName: string;
  tLogFilename: string;
begin
  LogString('Start loading proxy settings', TPath.GetFullPath(ProxyIniFileName), LOGTYPE_DEBUG);

  ProxyIniObject := TMemIniFile.Create(TPath.GetFullPath(ProxyIniFilename));

  AllSections := TStringList.Create;
  ProxyIniObject.ReadSections(AllSections);

  LogString('Creating ProxyConnectionsList', '', LOGTYPE_DEBUG);
  ProxyConnectionsList := TList.Create;

  for I := 0 to AllSections.Count-1 do
  begin
    if SameText(LeftStr(AllSections[I], Length(CONNECTION_PREFIX)), CONNECTION_PREFIX) then
    begin
      bSkipThisSection := False;

      iSectionName := AllSections[I].Substring(Length(CONNECTION_PREFIX));

      LogString('Section #' + IntToStr(i), '''' + iSectionName + '''', LOGTYPE_DEBUG);

      // name of proxy entry
      tName := ProxyIniObject.ReadString(AllSections[i], IDENT_NAME, iSectionName);

      // log filename template
      tLogFilename :=
        TPath.GetFullPath(
          ProxyIniObject.ReadString(
            AllSections[I],
            IDENT_LOGFILENAME,
            (
              LeftStr(
                GlobalLogFullFilenameTemplate,
                Length(GlobalLogFullFilenameTemplate) - Length(ExtractFileExt(GlobalLogFullFilenameTemplate))
              ) + '[' + tName + ']' + ExtractFileExt(GlobalLogFullFilenameTemplate)
            )
          )
        );

      iCurrentListIndex :=
        ProxyConnectionsList.Add(
          TProxyConnection.Create(
            ProxyIniObject.ReadString(AllSections[I], IDENT_NAME, iSectionName),
            tLogFilename,
            GlobalLogLevel
          )
        );

      sCurrentProxyTypeString := ProxyIniObject.ReadString(AllSections[I], IDENT_PROXYTYPE, PROXYTYPE_INCORRECT);
      vCurrentProxyType := TProxyType.TypeIncorrect;

      if SameText(sCurrentProxyTypeString, PROXYTYPE_TCPTOTCP) then
      begin
        // this is TcpToTcp
        vCurrentProxyType := TProxyType.TcpToTcp;
        TProxyConnection(ProxyConnectionsList[iCurrentListIndex]).ProxyType := vCurrentProxyType;
      end
      else
      begin
        if SameText(sCurrentProxyTypeString, PROXYTYPE_TCPTO1C) then
        begin
          // this is TcpTo1C
          vCurrentProxyType := TProxyType.TcpTo1C;
          TProxyConnection(ProxyConnectionsList[iCurrentListIndex]).ProxyType := vCurrentProxyType;
        end
      end;

      TProxyConnection(ProxyConnectionsList[iCurrentListIndex]).ListenIP := ProxyIniObject.ReadString(AllSections[I], IDENT_LISTENIP, DEFAULT_LISTENIP);

      TProxyConnection(ProxyConnectionsList[iCurrentListIndex]).ListenPort := ProxyIniObject.ReadInteger(AllSections[I], IDENT_LISTENPORT, DEFAULT_LISTENPORT);
      if TProxyConnection(ProxyConnectionsList[iCurrentListIndex]).ListenPort = INCORRECT_LISTENPORT then
      begin
        // incorrect listen port
        LogString('Incorrect listen port specified, skipping section', LOGTYPE_DEBUG);
        bSkipThisSection := True;
      end;

      TProxyConnection(ProxyConnectionsList[iCurrentListIndex]).EndOfMessage := StrEscapedToNormal(ProxyIniObject.ReadString(AllSections[I], IDENT_ENDOFMESSAGE, DEFAULT_ENDOFMESSAGE));
      TProxyConnection(ProxyConnectionsList[iCurrentListIndex]).SendEndOfMessage := ProxyIniObject.ReadBool(AllSections[I], IDENT_SENDENDOFMESSAGE, DEFAULT_SENDENDOFMESSAGE);

      case vCurrentProxyType of
        TProxyType.TcpToTcp:
          begin
            TProxyConnection(ProxyConnectionsList[iCurrentListIndex]).ListenOutbound :=
              ProxyIniObject.ReadBool(AllSections[I], IDENT_LISTENOUTBOUND, DEFAULT_LISTENOUTBOUND);

            if TProxyConnection(ProxyConnectionsList[iCurrentListIndex]).ListenOutbound then
            begin
              // if we must listen outbound
              TProxyConnection(ProxyConnectionsList[iCurrentListIndex]).ListenOutboundIP :=
                ProxyIniObject.ReadString(AllSections[I], IDENT_LISTENOUTBOUNDIP, DEFAULT_LISTENOUTBOUNDIP);

              TProxyConnection(ProxyConnectionsList[iCurrentListIndex]).ListenOutboundPort :=
                ProxyIniObject.ReadInteger(AllSections[I], IDENT_LISTENOUTBOUNDPORT, DEFAULT_LISTENOUBOUNDPORT);
              if TProxyConnection(ProxyConnectionsList[iCurrentListIndex]).ListenOutboundPort = INCORRECT_LISTENPORT then
              begin
                // incorrect listen outbound port
                LogString('Incorrect listen port for outbound specified, skipping section');
                bSkipThisSection := True;
              end;

              TProxyConnection(ProxyConnectionsList[iCurrentListIndex]).ListenOutboundStatusString :=
                ProxyIniObject.ReadString(AllSections[I], IDENT_LISTENOUTBOUNDSTATUSSTRING, DEFAULT_LISTENOUTBOUNDSTATUSSTRING);
            end
            else
            begin
              // if we must connect to outbound
              TProxyConnection(ProxyConnectionsList[iCurrentListIndex]).OutServer := ProxyIniObject.ReadString(AllSections[I], IDENT_OUTSERVER, DEFAULT_OUTSERVER);
              if TProxyConnection(ProxyConnectionsList[iCurrentListIndex]).OutServer = INCORRECT_OUTSERVER then
              begin
                LogString('Incorrect outgoing host specified, skipping section', LOGTYPE_DEBUG);
                bSkipThisSection := True;
              end;

              TProxyConnection(ProxyConnectionsList[iCurrentListIndex]).OutPort := ProxyIniObject.ReadInteger(AllSections[I], IDENT_OUTPORT, DEFAULT_OUTPORT);
              if TProxyConnection(ProxyConnectionsList[iCurrentListIndex]).OutPort = INCORRECT_OUTPORT then
              begin
                LogString('Incorrect outgoing port specified, skipping section', LOGTYPE_DEBUG);
                bSkipThisSection := True;
              end;
            end;

            TProxyConnection(ProxyConnectionsList[iCurrentListIndex]).RestoreIfDisconnect := ProxyIniObject.ReadBool(AllSections[I], IDENT_RESTOREIFDISCONNECT, DEFAULT_RESTOREIFDISCONNECT);
            TProxyConnection(ProxyConnectionsList[iCurrentListIndex]).RestoreAttempts := ProxyIniObject.ReadInteger(AllSections[I], IDENT_RESTOREATTEMPTS, DEFAULT_RESTOREATTEMPTS);
            if TProxyConnection(ProxyConnectionsList[iCurrentListIndex]).RestoreAttempts = 0 then
              TProxyConnection(ProxyConnectionsList[iCurrentListIndex]).RestoreAttempts := DEFAULT_RESTOREATTEMPTS;

            TProxyConnection(ProxyConnectionsList[iCurrentListIndex]).Passthrough := ProxyIniObject.ReadBool(AllSections[I], IDENT_PASSTHROUGH, DEFAULT_PASSTHROUGH);
            TProxyConnection(ProxyConnectionsList[iCurrentListIndex]).ResponseTimeout := ProxyIniObject.ReadInteger(AllSections[I], IDENT_RESPONSETIMEOUT, DEFAULT_RESPONSETIMEOUT);

            TProxyConnection(ProxyConnectionsList[iCurrentListIndex]).ConnectionCheckPeriod := ProxyIniObject.ReadInteger(AllSections[I], IDENT_CONNECTIONCHECKPERIOD, DEFAULT_CONNECTIONCHECKPERIOD);

            TProxyConnection(ProxyConnectionsList[iCurrentListIndex]).PersistentConnection := ProxyIniObject.ReadBool(AllSections[I], IDENT_PERSISTENTCONNECTION, DEFAULT_PERSISTENTCONNECTION);

            TProxyConnection(ProxyConnectionsList[iCurrentListIndex]).InboundTimeout :=
              ProxyIniObject.ReadInteger(AllSections[I], IDENT_INBOUNDTIMEOUT, DEFAULT_INBOUNDTIMEOUT);
            TProxyConnection(ProxyConnectionsList[iCurrentListIndex]).DropInboundBufferOnTimeout :=
              ProxyIniObject.ReadBool(AllSections[I], IDENT_DROPINBOUNDBUFFERONTIMEOUT, DEFAULT_DROPINBOUNDBUFFERONTIMEOUT);
            TProxyConnection(ProxyConnectionsList[iCurrentListIndex]).ProcessInboundBufferOnTimeout :=
              ProxyIniObject.ReadBool(AllSections[I], IDENT_PROCESSINBOUNDBUFFERONTIMEOUT, DEFAULT_PROCESSINBOUNDBUFFERONTIMEOUT);

            TProxyConnection(ProxyConnectionsList[iCurrentListIndex]).OnShutdownInboundTimeout :=
              ProxyIniObject.ReadInteger(AllSections[I], IDENT_ONSHUTDOWNINBOUNDTIMEOUT, DEFAULT_ONSHUTDOWNINBOUNDTIMEOUT);
            TProxyConnection(ProxyConnectionsList[iCurrentListIndex]).OnShutdownOutboundTimeout :=
              ProxyIniObject.ReadInteger(AllSections[I], IDENT_ONSHUTDOWNOUTBOUNDTIMEOUT, DEFAULT_ONSHUTDOWNOUTBOUNDTIMEOUT);
            TProxyConnection(ProxyConnectionsList[iCurrentListIndex]).OnShutdownSendInboundBuffer :=
              ProxyIniObject.ReadBool(AllSections[I], IDENT_ONSHUTDOWNSENDINBOUNDBUFFER, DEFAULT_ONSHUTDOWNSENDINBOUNDBUFFER);
            TProxyConnection(ProxyConnectionsList[iCurrentListIndex]).OnShutdownSendOutboundBuffer :=
              ProxyIniObject.ReadBool(AllSections[I], IDENT_ONSHUTDOWNSENDOUTBOUNDBUFFER, DEFAULT_ONSHUTDOWNSENDOUTBOUNDBUFFER);
          end;

        TProxyType.TcpTo1C:
          begin
            TProxyConnection(ProxyConnectionsList[iCurrentListIndex]).InitStr := ProxyIniObject.ReadString(AllSections[I], IDENT_INITSTR, DEFAULT_INITSTR);
            if TProxyConnection(ProxyConnectionsList[iCurrentListIndex]).InitStr = INCORRECT_INITSTR then
            begin
              LogString('Incorrect 1C init string specified, skipping section', LOGTYPE_DEBUG);
              bSkipThisSection := True;
            end;

            TProxyConnection(ProxyConnectionsList[iCurrentListIndex]).CallbackFunction := ProxyIniObject.ReadString(AllSections[I], IDENT_CALLBACKFUNCTION, DEFAULT_CALLBACKFUNCTION);
            if TProxyConnection(ProxyConnectionsList[iCurrentListIndex]).CallbackFunction = INCORRECT_CALLBACKFUNCTION then
            begin
              LogString('Incorrect 1C callback function specified, skipping section', LOGTYPE_DEBUG);
              bSkipThisSection := True;
            end;

            TProxyConnection(ProxyConnectionsList[iCurrentListIndex]).ConvertString := ProxyIniObject.ReadBool(AllSections[I], IDENT_CONVERTSTRING, DEFAULT_CONVERTSTRING);

            TProxyConnection(ProxyConnectionsList[iCurrentListIndex]).ConnectTo1Cv8 := nil;
          end;

        TProxyType.TypeIncorrect:
          begin
            // this is incorrect
            // delete current item in list then skip this section
            LogString('Incorrect type of connection specified, skipping section', LOGTYPE_DEBUG);
            bSkipThisSection := True;
          end;
      end;

      if bSkipThisSection then
      begin
        TProxyConnection(ProxyConnectionsList[iCurrentListIndex]).Free;
        ProxyConnectionsList.Delete(iCurrentListIndex);
        Continue;
      end;

    end;
  end;

  AllSections.Free;
  ProxyIniObject.Free;

// pack and free unused memory in ProxyConnectionsList
  ProxyConnectionsList.Pack;
  ProxyConnectionsList.Capacity := ProxyConnectionsList.Count;

  LogString('Dumping connections', 'Total count: ' + IntToStr(ProxyConnectionsList.Count), LOGTYPE_DEBUG);
  for I := 0 to (ProxyConnectionsList.Count - 1) do
  begin
    LogString('Connection #' + IntToStr(I), 'Name: ' + TProxyConnection(ProxyConnectionsList[I]).Name, LOGTYPE_DEBUG);
    LogString('..', 'Listen IP: ' + TProxyConnection(ProxyConnectionsList[I]).ListenIP, LOGTYPE_DEBUG);
    LogString('..', 'Listen port: ' + IntToStr(TProxyConnection(ProxyConnectionsList[I]).ListenPort), LOGTYPE_DEBUG);
    LogString('..', 'End of message: ' + StrNormalToEscaped(TProxyConnection(ProxyConnectionsList[I]).EndOfMessage), LOGTYPE_DEBUG);
    case TProxyConnection(ProxyConnectionsList[I]).ProxyType  of
      TProxyType.TcpToTcp:
        begin
          LogString('..', 'Proxy type: TcpTo1C', LOGTYPE_DEBUG);
          LogString('..', 'Out server: ' + TProxyConnection(ProxyConnectionsList[I]).OutServer, LOGTYPE_DEBUG);
          LogString('..', 'Out port: ' + IntToStr(TProxyConnection(ProxyConnectionsList[I]).OutPort), LOGTYPE_DEBUG);
          LogString('..', 'Restore if disconnect: ' + BoolToStr(TProxyConnection(ProxyConnectionsList[I]).RestoreIfDisconnect, True), LOGTYPE_DEBUG);
          LogString('..', 'Restore attempts: ' + IntToStr(TProxyConnection(ProxyConnectionsList[I]).RestoreAttempts), LOGTYPE_DEBUG);
          LogString('..', 'Passthrough: ' + BoolToStr(TProxyConnection(ProxyConnectionsList[I]).Passthrough, True), LOGTYPE_DEBUG);
        end;

      TProxyType.TcpTo1C:
        begin
          LogString('..', 'Proxy type: TcpToTcp', LOGTYPE_DEBUG);
          LogString('..', 'InitStr: "' + TProxyConnection(ProxyConnectionsList[I]).InitStr + '"', LOGTYPE_DEBUG);
          LogString('..', 'CallbackFunction: "' + TProxyConnection(ProxyConnectionsList[I]).CallbackFunction + '"', LOGTYPE_DEBUG);
        end;
    end;

  end;

  LogString('Proxy settings loaded OK', '', LOGTYPE_DEBUG);

end;

procedure StartWorking(AOwnerService: Vcl.SvcMgr.TService = nil);
var
  I: Integer;
  iBinding: TIdSocketHandle;
  iOutBinding: TIdSocketHandle;
begin
  if ProxyConnectionsList.Count < 1 then
    Exit;

  LogString('Start configuring TCP server', '', LOGTYPE_DEBUG);

  for I := 0 to (ProxyConnectionsList.Count - 1) do
  begin
    TProxyConnection(ProxyConnectionsList[I]).PrepareIdTcpServer;

    iBinding := TProxyConnection(ProxyConnectionsList[I]).IdTcpServer.Bindings.Add;
    iBinding.IP := TProxyConnection(ProxyConnectionsList[I]).ListenIP;
    iBinding.Port := TProxyConnection(ProxyConnectionsList[I]).ListenPort;

    LogString('Added binding', iBinding.IP + ':' + IntToStr(iBinding.Port), LOGTYPE_DEBUG);
  end;

  for I := 0 to (ProxyConnectionsList.Count - 1) do
  begin
    case TProxyConnection(ProxyConnectionsList[I]).ProxyType of
      TProxyType.TcpTo1C:
        begin
          LogString('Trying to connect to 1C', 'InitStr=' + TProxyConnection(ProxyConnectionsList[I]).InitStr, LOGTYPE_DEBUG);
          TProxyConnection(ProxyConnectionsList[I]).ConnectTo1Cv8 := TConnectTo1Cv8.CreateAndConnectByInitStr(TProxyConnection(ProxyConnectionsList[I]).InitStr);
          if TProxyConnection(ProxyConnectionsList[I]).ConnectTo1Cv8.V8 <> nil then
            LogString('Connected to 1C', 'InitStr=' + TProxyConnection(ProxyConnectionsList[I]).InitStr, LOGTYPE_DEBUG)
          else
            LogString('Failure connect to 1C', 'InitStr=' + TProxyConnection(ProxyConnectionsList[I]).InitStr, LOGTYPE_DEBUG);
        end;

      TProxyType.TcpToTcp:
        begin
          if TProxyConnection(ProxyConnectionsList[I]).ListenOutbound then
          begin
            // start a server (outbound)
            LogString('Trying to start a TCP server (outbound)', TProxyConnection(ProxyConnectionsList[I]).ListenOutboundIP + ':' + IntToStr(TProxyConnection(ProxyConnectionsList[I]).ListenOutboundPort), LOGTYPE_DEBUG);

            TProxyConnection(ProxyConnectionsList[I]).PrepareOutboundServer();
            iOutBinding := TProxyConnection(ProxyConnectionsList[I]).OutboundServer.Bindings.Add();
            iOutBinding.IP := TProxyConnection(ProxyConnectionsList[I]).ListenOutboundIP;
            iOutBinding.Port := TProxyConnection(ProxyConnectionsList[I]).ListenOutboundPort;

            // trying to start outbound server
            TProxyConnection(ProxyConnectionsList[I]).OutboundServer.Active := True;
            LogString('Outbound TCP server started successfully', TProxyConnection(ProxyConnectionsList[I]).ListenOutboundIP + ':' + IntToStr(TProxyConnection(ProxyConnectionsList[I]).ListenOutboundPort), LOGTYPE_DEBUG);
          end
          else
          begin
            // start a client (outbound)
            LogString('Trying to connect to TCP', TProxyConnection(ProxyConnectionsList[I]).OutServer + ':' + IntToStr(TProxyConnection(ProxyConnectionsList[I]).OutPort), LOGTYPE_DEBUG);

            TProxyConnection(ProxyConnectionsList[I]).PrepareIdTcpClient;
            TProxyConnection(ProxyConnectionsList[I]).IdTcpClient.Host := TProxyConnection(ProxyConnectionsList[I]).OutServer;
            TProxyConnection(ProxyConnectionsList[I]).IdTcpClient.Port := TProxyConnection(ProxyConnectionsList[I]).OutPort;

            if TProxyConnection(ProxyConnectionsList[I]).PersistentConnection then
            begin

              try
                TProxyConnection(ProxyConnectionsList[I]).IdTcpClient.Connect;
                if TProxyConnection(ProxyConnectionsList[I]).IdTcpClient.Connected then
                begin
                  LogString('Successfully connected to TCP',
                    TProxyConnection(ProxyConnectionsList[I]).IdTcpClient.BoundIP + ':' + IntToStr(TProxyConnection(ProxyConnectionsList[I]).IdTcpClient.BoundPort) +
                    '->' +
                    TProxyConnection(ProxyConnectionsList[I]).IdTcpClient.Socket.Host + ':' + IntToStr(TProxyConnection(ProxyConnectionsList[I]).IdTcpClient.Socket.Port),
                    LOGTYPE_DEBUG);
                end
                else
                  begin
                    LogString('Failure connecting, will try later', LOGTYPE_DEBUG);
                    TProxyConnection(ProxyConnectionsList[I]).RestoreAttemptsRemained := TProxyConnection(ProxyConnectionsList[I]).RestoreAttemptsRemained - 1;
                  end;
              except
                on E: Exception do
                  begin
                    LogString('Exception ' + E.ClassName, E.Message, LOGTYPE_ERROR);
                    LogString('Failure connecting, will try later', LOGTYPE_DEBUG);
                    TProxyConnection(ProxyConnectionsList[I]).RestoreAttemptsRemained := TProxyConnection(ProxyConnectionsList[I]).RestoreAttemptsRemained - 1;
                  end;
              end;

            end;
          end;
        end;
    end;
  end;

  for I := 0 to (ProxyConnectionsList.Count - 1) do
  begin
    LogString('Trying to start TCP server listeners', '', LOGTYPE_DEBUG);
    TProxyConnection(ProxyConnectionsList[I]).IdTcpServer.Active := True;
    LogString('TCP server started successfully', '', LOGTYPE_DEBUG);
  end;
end;

procedure StopWorking(AOwnerService: Vcl.SvcMgr.TService = nil);
var
  i: Integer;
begin
  for I := 0 to ProxyConnectionsList.Count - 1 do
  begin
    TProxyConnection(ProxyConnectionsList[I]).Free;
  end;
end;

initialization

finalization

FreeAndNil(ProxyConnectionsList);

end.
