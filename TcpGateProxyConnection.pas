unit TcpGateProxyConnection;

interface

uses
  System.SysUtils, System.StrUtils, System.Win.ComObj, System.Win.ObjComAuto, System.Classes,
  Winapi.Windows, Winapi.ActiveX,
  Web.Win.Sockets, ConnectTo1Cv8,
  Vcl.ExtCtrls,
  IdGlobal, IdTcpServer, IdTcpClient, IdSocketHandle, IdContext, IdComponent,
  TcpGateHelpers,
  TcpGateLogUnit, TcpGateCommon;

type
  TProxyType = (
    TcpToTcp = 1,
    TcpTo1C = 2,
    TypeIncorrect = -1
  );

type
  TCustomProxyConnection = class

  end;

type
  TProxyConnection = class(TCustomProxyConnection)
  private
    FRestoreAttemptsRemained: Integer;
    FRestoreAttempts: Integer;

    procedure SetRestoreAttempts(Value: Integer);
  public
    conlog: TConnectionLog;
    Name: string;
    ProxyType: TProxyType;
    ListenIP: string;
    ListenPort: Integer; // 0..65535, 16-bit unsigned
    EndOfMessage: string;
    SendEndOfMessage: Boolean;

    // TcpToTcp only
    OutServer: string;   // TcpToTcp only
    OutPort: Integer;         // TcpToTcp only
    RestoreIfDisconnect: Boolean; //TcpToTcp only
    Passthrough: Boolean;         //TcpToTcp only
    ResponseTimeout: Integer; // 10
    ConnectionCheckPeriod: Integer; // 10

    PersistentConnection: Boolean; // True

    ListenOutbound: Boolean;
    ListenOutboundIP: string;
    ListenOutboundPort: Integer;
    ListenOutboundStatusString: string;

    OutboundServer: TIdTcpServer;

    // TcpTo1C only
    InitStr: string;          // TcpTo1C only
    CallbackFunction: string; // TcpTo1C only
    ConvertString: Boolean; // TcpTo1C only

// common
    ConnectTo1Cv8: TConnectTo1Cv8; // TcpTo1C only
    IdTcpServer: TIdTcpServer;
    IdTcpClient: TIdTcpClient; // TcpToTcp only
    ClientTimer: TTimer;

    InboundTimeout: Integer;
    DropInboundBufferOnTimeout: Boolean;
    ProcessInboundBufferOnTimeout: Boolean;

    OnShutdownInboundTimeout: Integer;
    OnShutdownOutboundTimeout: Integer;
    OnShutdownSendInboundBuffer: Boolean;
    OnShutdownSendOutboundBuffer: Boolean;

    constructor Create(const vName: string; const vLogFilenameTemplate: string; vLogLevel: Integer);
    destructor Destroy; override;
    procedure PrepareIdTcpServer;
    procedure PrepareIdTcpClient;
    procedure PrepareOutboundServer;
    procedure ProxyConnectionTcpServerExecute(AContext: TIdContext);
    procedure ProxyConnectionTcpServerConnect(AContext: TIdContext);
    procedure ProxyConnectionTcpServerDisconnect(AContext: TIdContext);

    procedure ProxyConnectionOutboundServerExecute(AContext: TIdContext);
    procedure ProxyConnectionOutboundServerConnect(AContext: TIdContext);
    procedure ProxyConnectionOutboundServerDisconnect(AContext: TIdContext);

    procedure ProcessMessage(s: AnsiString; AContext: TIdContext); overload;
    procedure ProcessMessage(AByte: Byte; AContext: TIdContext); overload;
    procedure SendMessageToTcp(s: AnsiString; AContext: TIdContext);
    procedure SendMessageTo1C(s: AnsiString; AContext: TIdContext);
    procedure WaitAndResendResponse(AContext: TIdContext);

    function IsAnyoneConnectedToOutboundServer: Boolean;
    function GetOutboundServerFirstClient: TIdContext;

    procedure ProxyConnectionTcpClientStatus(ASender: TObject; const AStatus: TIdStatus; const AStatusText: string);

    procedure ProxyConnectionTimerOnTimer(Sender: TObject);

    procedure TryToReconnect();

    property RestoreAttempts: Integer read FRestoreAttempts write SetRestoreAttempts;     //TcpToTcp only
    property RestoreAttemptsRemained: Integer read FRestoreAttemptsRemained write FRestoreAttemptsRemained; // TcpToTcp only
  end;

implementation

constructor TProxyConnection.Create(const vName: string; const vLogFilenameTemplate: string; vLogLevel: Integer);
begin
  inherited Create;

  Self.Name := vName;

  Self.IdTcpServer := nil;
  Self.IdTcpClient := nil;
  Self.OutboundServer := nil;
  Self.ClientTimer := nil;

  Self.ListenOutbound := False;

  Self.conlog := TConnectionLog.Create(vLogFilenameTemplate, vLogLevel);
end;

destructor TProxyConnection.Destroy;
//var
//  I: Integer;
begin
  Self.ClientTimer.Enabled := False;
  Self.conlog.Free;

  // Send remaining inbound buffer
  if (Self.IdTcpServer <> nil) then
  begin
    if Self.OnShutdownSendInboundBuffer then
    begin

    end;
  end;

  inherited;
end;

function TProxyConnection.GetOutboundServerFirstClient: TIdContext;
var
  ClientsList: TList;
begin
  Result := nil;
  if Self.IsAnyoneConnectedToOutboundServer then
  begin
    ClientsList := Self.OutboundServer.Contexts.LockList;

    try
      Result := TIdContext(ClientsList[0]);
    finally
      Self.OutboundServer.Contexts.UnlockList;
    end;
  end;
end;

function TProxyConnection.IsAnyoneConnectedToOutboundServer: Boolean;
var
  ClientsList: TList;
begin
  Result := False;

  if Self.ListenOutbound then
  begin
    if Self.OutboundServer <> nil then
    begin
      if Self.OutboundServer.Active then
      begin
        ClientsList := Self.OutboundServer.Contexts.LockList;
        try
          Result := ClientsList.Count > 0;
        finally
          Self.OutboundServer.Contexts.UnlockList;
        end;
      end;
    end;
  end;
end;

procedure TProxyConnection.PrepareIdTcpClient;
begin
  Self.IdTcpClient := TIdTcpClient.Create();
  Self.IdTcpClient.OnStatus := Self.ProxyConnectionTcpClientStatus;

  Self.ClientTimer := TTimer.Create(nil);
  Self.ClientTimer.Interval := Self.ConnectionCheckPeriod;
  Self.ClientTimer.OnTimer := Self.ProxyConnectionTimerOnTimer;
  Self.ClientTimer.Enabled := True;
end;

procedure TProxyConnection.PrepareIdTcpServer;
begin
  Self.IdTcpServer := TIdTcpServer.Create();
  Self.IdTcpServer.Bindings.Clear;
  Self.IdTcpServer.OnConnect := Self.ProxyConnectionTcpServerConnect;
  Self.IdTcpServer.OnExecute := Self.ProxyConnectionTcpServerExecute;
  Self.IdTcpServer.OnDisconnect := Self.ProxyConnectionTcpServerDisconnect;
end;

procedure TProxyConnection.PrepareOutboundServer;
begin
  Self.OutboundServer := TIdTcpServer.Create();
  Self.OutboundServer.Bindings.Clear();
  Self.OutboundServer.MaxConnections := 1;
  Self.OutboundServer.OnConnect := Self.ProxyConnectionOutboundServerConnect;
  Self.OutboundServer.OnExecute := Self.ProxyConnectionOutboundServerExecute;
  Self.OutboundServer.OnDisconnect := Self.ProxyConnectionTcpServerDisconnect;
end;

procedure TProxyConnection.ProcessMessage(s: AnsiString; AContext: TIdContext);
begin
  conlog.LogString('>>>','ProcessMessage(s)',LOGTYPE_EXTRA);
  if s <> '' then
  begin
    conlog.LogString('Got message', string(s), Self.Name, LOGTYPE_DEBUG);
    conlog.LogString('Dump of message', '^' + StrNormalToEscaped(string(s)) + '$', LOGTYPE_EXTRA);
    case Self.ProxyType of
      TProxyType.TcpToTcp:
        begin
          conlog.LogString('Transit traffic', 'Trying to send', Self.Name, LOGTYPE_DEBUG);
          Self.SendMessageToTcp(s, AContext);
        end;

      TProxyType.TcpTo1C:
        begin
          conlog.LogString('Traffic to 1C', 'Trying to send', Self.Name, LOGTYPE_DEBUG);
          Self.SendMessageTo1C(s, AContext);
        end;
    end;
  end else begin
    conlog.LogString('Empty message! Skipping', LOGTYPE_DEBUG);
  end;
  conlog.LogString('<<<','ProcessMessage(s)',LOGTYPE_EXTRA);
end;

procedure TProxyConnection.ProcessMessage(AByte: Byte; AContext: TIdContext);
begin
  conlog.LogString('Transit traffic', 'Passthrough', Self.Name, LOGTYPE_INFO);
  conlog.LogString('Sending byte', IntToStr(Ord(AByte)), Self.Name, LOGTYPE_DEBUG);

  if Self.ListenOutbound then
  begin
    if Self.IsAnyoneConnectedToOutboundServer then
    begin

    end
    else
    begin
      conlog.LogString('No one connected to outbound listen server', '', Self.Name, LOGTYPE_DEBUG);
    end;
  end
  else
  begin

    if not Self.IdTcpClient.Connected then
    begin
      conlog.LogString('We are disconnected :-(', LOGTYPE_DEBUG);

      Self.TryToReconnect;
    end;

    if Self.IdTcpClient.Connected then
    begin
      // reset attempts counter
      Self.FRestoreAttemptsRemained := Self.FRestoreAttempts;

      conlog.LogString('Sending received byte', '', Self.Name, LOGTYPE_DEBUG);

      Self.IdTcpClient.IOHandler.Write(AByte);
    end;

    Self.WaitAndResendResponse(AContext);

  end;
end;

procedure TProxyConnection.WaitAndResendResponse(AContext: TIdContext);
var
  s: string;
begin
  conlog.LogString('>>>','WaitAndResendResponse', LOGTYPE_EXTRA);

  if Self.ListenOutbound then
  begin
    // outbound server

  end
  else
  begin
    // outbound client

    // check for response
    if Self.IdTcpClient.IOHandler.InputBufferIsEmpty then
    begin
      Self.IdTcpClient.IOHandler.CheckForDataOnSource(Self.ResponseTimeout);
    end;

    if Self.IdTcpClient.IOHandler.InputBufferIsEmpty then
    begin
      // send TIMEOUT
      AContext.Connection.IOHandler.WriteLn(MSG_RESPONSE_TIMEOUT);

      conlog.LogString(MSG_TRANSIT_TRAFFIC, 'Timeout waiting for response', Self.Name, LOGTYPE_ERROR);
    end else begin
      // resend buffer
      s := Self.IdTcpClient.IOHandler.InputBufferAsString();
      //����; �����;<info>; <������������� ���� ������>; Got answer;<����� �� ���������>
      conlog.LogString('Got answer', s, Self.Name, LOGTYPE_INFO);

      AContext.Connection.IOHandler.Write(s);
      // ����; �����;<info>; <������������� ���� ������>; Sent answer;
      conlog.LogString('Sent answer', s, Self.Name, LOGTYPE_INFO);

      conlog.LogString('Response string: ', '"' + s + '"', Self.Name, LOGTYPE_DEBUG);
    end;
  end;

  conlog.LogString('<<<','WaitAndResendResponse', LOGTYPE_EXTRA);
end;


procedure TProxyConnection.SendMessageTo1C(s: AnsiString; AContext: TIdContext);
var
  i1C: IDispatch;
  NameDispIds: array [0..0] of TDispID;
  Names: array [0..0] of PWideChar;
  ReturnValue: OleVariant;
  ExcepInfo: TExcepInfo;
  ParamError: OleVariant;
  Params: TDispParams;
  Args: array [0..1] of TVariantArg;
  sName: PWideChar;
  sData: PWideChar;
begin
  sName := PWideChar(Self.Name);
  sData := PWideChar(WideString(s));

  Args[0].vt := VT_BSTR;
  Args[0].bstrVal := sData;
  conlog.LogString('Length of data', IntToStr(ByteLength(sData)), Self.Name, LOGTYPE_DEBUG);

  Args[1].vt := VT_BSTR;
  Args[1].bstrVal := sName;

  FillChar(Params, SizeOf(Params), 0);
  Params.rgvarg := @Args;
  Params.rgdispidNamedArgs := nil;
  Params.cArgs := 2;
  Params.cNamedArgs := 0;

  i1C := Self.ConnectTo1Cv8.V8;

  Names[0] := StringToOleStr(Self.CallbackFunction);
  i1C.GetIDsOfNames(GUID_NULL, @Names, 1, LOCALE_SYSTEM_DEFAULT, @NameDispIds);

  try
    OleCheck(i1C.Invoke(NameDispIds[0], GUID_NULL, LOCALE_SYSTEM_DEFAULT, DISPATCH_METHOD, Params, @ReturnValue, @ExcepInfo, @ParamError));
  except
    on E: Exception do
    begin
      conlog.LogString('Exception ' + E.ClassName, E.Message, Self.Name, LOGTYPE_ERROR);
      conlog.LogString('  ExcepInfo.wCode', IntToStr(ExcepInfo.wCode), Self.Name, LOGTYPE_DEBUG);
      conlog.LogString('  ExcepInfo.bstrSource', ExcepInfo.bstrSource, Self.Name, LOGTYPE_DEBUG);
      conlog.LogString('  ExcepInfo.bstrDescription', ExcepInfo.bstrDescription, Self.Name, LOGTYPE_DEBUG);
    end;
  end;
end;

procedure TProxyConnection.SendMessageToTcp(s: AnsiString; AContext: TIdContext);
var
  idFirstClientContext: TIdContext;
begin
  if Self.ListenOutbound then
  begin
    // we must send to first and only client, connected to outbound server

    if Self.IsAnyoneConnectedToOutboundServer then
    begin
      idFirstClientContext := Self.GetOutboundServerFirstClient();
      idFirstClientContext.Connection.IOHandler.Write(string(s));
    end
    else
    begin
      conlog.LogString('No one connected to outbound listen server', '', Self.Name, LOGTYPE_DEBUG);
    end;

  end
  else
  begin
    if not Self.IdTcpClient.Connected then
    begin
      conlog.LogString('We are disconnected', 'Trying to reconnect', Self.Name, LOGTYPE_ERROR);

      Self.TryToReconnect;
    end;

    if Self.IdTcpClient.Connected then
    begin
      // reset attempts counter
      conlog.LogString('Reset remained attempts counter', '', Self.Name, LOGTYPE_DEBUG);
      Self.FRestoreAttemptsRemained := Self.FRestoreAttempts;

      conlog.LogString('Sending received string', '', Self.Name, LOGTYPE_DEBUG);
      try
        Self.IdTcpClient.IOHandler.Write(string(s));
        // ����; �����;<info>; <������������� ���� ������>; Sent to receiver;>
        conlog.LogString('Sent to receiver', '', Self.Name, LOGTYPE_INFO);
      except
        on E: Exception do
        begin
          conlog.LogString('Exception ' + E.ClassName, E.Message, Self.Name, LOGTYPE_ERROR);
        end;
      end;

      if Self.SendEndOfMessage then
      begin
        Self.IdTcpClient.IOHandler.Write(Self.EndOfMessage);
      end;
    end;

    Self.WaitAndResendResponse(AContext);
  end;
end;

procedure TProxyConnection.SetRestoreAttempts(Value: Integer);
begin
  FRestoreAttempts := Value;
  FRestoreAttemptsRemained := Value;
end;

procedure TProxyConnection.ProxyConnectionTcpServerConnect(AContext: TIdContext);
begin
  conlog.LogString('Event OnConnect',
    AContext.Binding.IP + ':' + IntToStr(AContext.Binding.Port) +
    '<-' +
    AContext.Binding.PeerIP + ':' + IntToStr(AContext.Binding.PeerPort),
    Self.Name,
    LOGTYPE_DEBUG);

  conlog.LogString('Proxy connection: ' + Self.Name, '', Self.Name, LOGTYPE_DEBUG)
end;

procedure TProxyConnection.ProxyConnectionTcpServerExecute(AContext: TIdContext);
var
  s: AnsiString;
  wasSplit: Boolean;
  b: Byte;
  process: Boolean;
begin
  conlog.LogString('>>>', '..Execute', LOGTYPE_EXTRA);

  if Self.Passthrough then
  begin
    b := AContext.Connection.IOHandler.ReadByte;
    Self.ProcessMessage(b, AContext);
  end
  else
  begin
    s := AnsiString(AContext.Connection.IOHandler.ReadLnSplit(wasSplit, Self.EndOfMessage, Self.InboundTimeout));

    process := (s <> '');

    if Self.ListenOutbound then
    begin
      if s = AnsiString(Self.ListenOutboundStatusString) then
      begin
        process := False;

        if Self.IsAnyoneConnectedToOutboundServer() then
          AContext.Connection.IOHandler.Write(MSG_STATUS_OPEN)
        else
          AContext.Connection.IOHandler.Write(MSG_STATUS_CLOSED);
      end;
    end;

    if process then
    begin
      // ����; �����;<info>; <������������� ���� ������>; Incoming;<���������� ���������>
      conlog.LogString('Incoming', string(s), Self.Name, LOGTYPE_INFO);
      Self.ProcessMessage(s, AContext);
    end;
  end;
  conlog.LogString('<<<', '..Execute', LOGTYPE_EXTRA);
end;

procedure TProxyConnection.ProxyConnectionTcpServerDisconnect(AContext: TIdContext);
begin
  conlog.LogString('Event OnDisconnect', '', Self.Name, LOGTYPE_DEBUG);
end;

procedure TProxyConnection.TryToReconnect;
begin
  conlog.LogString(IntToStr(Self.FRestoreAttemptsRemained), '', Self.Name, LOGTYPE_DEBUG);

  if Self.ListenOutbound then
  begin

  end
  else
  begin
    while (Self.FRestoreAttemptsRemained > 0) and (not Self.IdTcpClient.Connected) do
    begin
      Self.FRestoreAttemptsRemained := Self.FRestoreAttemptsRemained - 1;
      try
        conlog.LogString('Trying to reconnect', '', Self.Name, LOGTYPE_DEBUG);
        Self.IdTcpClient.Connect;
      except
        on E: Exception do
        begin
          conlog.LogString('Exception ' + E.ClassName, E.Message, Self.Name, LOGTYPE_ERROR);
          conlog.LogString('Failured to reconnect', 'Attempts remained: ' + IntToStr(Self.FRestoreAttemptsRemained), Self.Name, LOGTYPE_DEBUG);
        end;
      end;
    end;
  end;
end;

procedure TProxyConnection.ProxyConnectionOutboundServerConnect(
  AContext: TIdContext);
begin
  conlog.LogString('Outbound Server OnConnect',
    AContext.Binding.IP + ':' + IntToStr(AContext.Binding.Port) +
    '<-' +
    AContext.Binding.PeerIP + ':' + IntToStr(AContext.Binding.PeerPort),
    Self.Name,
    LOGTYPE_DEBUG);

  conlog.LogString('Proxy connection: ' + Self.Name, '', Self.Name, LOGTYPE_DEBUG)
end;

procedure TProxyConnection.ProxyConnectionOutboundServerDisconnect(
  AContext: TIdContext);
begin
  conlog.LogString('Outbound Server OnDisconnect', '', Self.Name, LOGTYPE_DEBUG);
end;

procedure TProxyConnection.ProxyConnectionOutboundServerExecute(
  AContext: TIdContext);
var
  s: AnsiString;
  wasSplit: Boolean;
  inboundClientsList: TList;
  firstInboundClient: TIdContext;
begin
  conlog.LogString('>>>', '..OutboundServerExecute', LOGTYPE_EXTRA);

  if Self.ListenOutbound then
  begin
    s := AnsiString(AContext.Connection.IOHandler.ReadLnSplit(wasSplit, Self.EndOfMessage));

    if (s <> '') then
    begin
      // send it back to inbound server
      inboundClientsList := Self.IdTcpServer.Contexts.LockList();

      try
        if inboundClientsList.Count > 0 then
          firstInboundClient := TIdContext(inboundClientsList[0])
        else
          firstInboundClient := nil;

        if firstInboundClient <> nil then
          firstInboundClient.Connection.IOHandler.Write(string(s));
      finally
        Self.IdTcpServer.Contexts.UnlockList();
      end;
    end;
  end;

  conlog.LogString('<<<', '..OutboundServerExecute', LOGTYPE_EXTRA);
end;

procedure TProxyConnection.ProxyConnectionTcpClientStatus(ASender: TObject; const AStatus: TIdStatus; const AStatusText: string);
begin
  conlog.LogString('OnStatus', AStatusText, Self.Name, LOGTYPE_DEBUG);

  if AStatus = TIdStatus.hsDisconnected then
  begin
    Self.TryToReconnect();
  end;
end;

procedure TProxyConnection.ProxyConnectionTimerOnTimer(Sender: TObject);
begin
  if Self.ListenOutbound then
  begin

  end
  else
  begin
    try
      if Self.IdTcpClient.Connected then
        Self.IdTcpClient.IOHandler.CheckForDataOnSource(Self.ConnectionCheckPeriod div 2);
    except
      on E: Exception do
      begin
        conlog.LogString('OnTimer!', '', Self.Name, LOGTYPE_DEBUG);
        conlog.LogString('Exception ' + E.ClassName, E.Message, Self.Name, LOGTYPE_DEBUG);
      end;
    end;
  end;
end;

end.
